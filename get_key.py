import requests,os,sys

key = os.getenv('key')
flask_url = os.getenv('flask_url')

if key == None:
    print("set variable environment key")
    print("example key=name")
    sys.exit()

data = {"key":key}
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
test = requests.get(flask_url)
try:
    if test.status_code == 200:
        r = requests.post(url=flask_url + '/get', headers=headers, json=data)
        print(r.text)
except:
    print('api down')
    sys.exit()

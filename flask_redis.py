import time

import redis,json,os
from flask import Flask,request, jsonify

redis_server = os.getenv('redis_server')
redis_port = os.getenv('redis_port')
app = Flask(__name__)
cache = redis.Redis(host=redis_server, port=redis_port)

def set_key(data):
    cache.set(data['key'],data['value'])

def get_key(key):
    value= cache.get(key['key'])
    return value

@app.route('/set', methods= ['POST'])
def set_redis():
    data = request.json
    set_key(data)

    return jsonify(data)

@app.route('/get', methods= ['POST'])
def get_redis():
    data = request.json
    value = get_key(data)
    return value


@app.route('/')
def hello():
    return 'Hello World!'

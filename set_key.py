import requests,os,sys

key = os.getenv('key')
value = os.getenv('value')
flask_url = os.getenv('flask_url')
if key == None or value == None:
    print('set environment key and value')
    print('example key=name value=john python3 set_key.py')
    sys.exit()

data = {"key":key,"value":value}
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
test = requests.get(flask_url)
try:
    if test.status_code == 200:
        r = requests.post(url=flask_url + '/set', headers=headers, json=data)
        print(r.json())
except:
    print('api down')
    sys.exit()
